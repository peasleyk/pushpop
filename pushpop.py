#!/usr/bin/python3

import requests
import os,sys
import json
import magic
import pprint
from pathlib import Path
import argparse


"""
  "simple" script  to send pushbullet a message
  Intended use is to send a completed message after a long command is run (dd, make, etc)
  Can send a file -f or just a message, and can set custom title and body with -t -b
  Key is hard coded as setup.py will install this to /usr/bin
  No external libraries required

  Future:
    - More push services if needed
    - More types of pushes
"""

KEY = ""

class pushBullet():
  """
    Small pushbullet class
    Can either send
      - a file (e.g. one created by the command)
      - a note
  """
  def __init__(self):

    self.pushes = "https://api.pushbullet.com/v2/pushes"
    self.upload = "https://api.pushbullet.com/v2/upload-request"
    self.BKEY = KEY

  def make_req(self,url,data=None,headers=None,files=None):

    """
      utility to make pushbullet api calls
    """

    if not headers:
      headers = {
        "Content-Type" : "application/json",
        "Access-Token" : self.BKEY
      }

    req = requests.request("POST",url,data=data,headers=headers,files=files)

    if not req.ok:
      print("Error: {} Push not sent.".format(req.json()['error']['message']))
      sys.exit()

    return req


  def send_note(self,title,body):
    """
      Sends a pushbullet title and body message
    """

    #Data that pushbullet asks for
    data = {
      "type" : "note",
      "title" : title,
      "body" : body
    }

    data = json.dumps(data)
    result = self.make_req(self.pushes,data)

  def send_file(self,body,filePath):
    """
      Sends a pushbullet body message and file
    """

    def getUploadLink(filePath):
      """
        We must get the URL to upload the file to , along with other data information
      """

      #Cool library to get full path and mime type
      filePath = str(Path(filePath).resolve())
      mime = magic.Magic(mime=True)
      fileType = mime.from_file(filePath)

      data = {
        "file_name" : filePath,
        "file_type" : fileType
      }

      data = json.dumps(data)
      result = self.make_req(self.upload,data)

      fileUrl = json.loads(result.text)["file_url"]
      fileName = json.loads(result.text)["file_name"]
      fileType = json.loads(result.text)["file_type"]
      fileUpload = json.loads(result.text)["upload_url"]

      return fileUrl,fileName,fileType,fileUpload


    def uploadFile(fileType,fileUpload):
      """
        fileUpload is the url that pushbullet gave us to upload out file to
        No special headers needed for this (information is in the url, fileUpload)
        No need to return anything as we're just uploading the file
      """

      files = {
        "file" : open(filePath,'rb')
      }
      result = requests.request("POST",fileUpload,files=files)


    #Get the url to upload the file to and upload it
    fileUrl,fileName,fileType,fileUpload = getUploadLink(filePath)
    uploadFile(fileType,fileUpload)

    #Finally send the push
    data = {
      "type" : "file",
      "body" : body,
      "file_name" : fileName,
      "file_type" : fileType,
      "file_url" : fileUrl
    }
    data = json.dumps(data)
    self.make_req(self.pushes,data)

def main(argv):

  parser = argparse.ArgumentParser()
  parser.add_argument("-f", "--file",
          help="send a file to be uploaded with pushbullet")
  parser.add_argument("-t", "--title",
          help="set the push title")
  parser.add_argument("-b", "--body",
          help="set the push body")

  parser.parse_args()
  results = vars(parser.parse_args())

  #Default messages
  title = "Finished!"
  fileTitle = "Command finished, view result"
  body = "The running command has finished"

  if results["title"]:
    title = results["title"]
    fileTitle = results["title"]
  if results["body"]:
    body = results["body"]

  pb = pushBullet()
  if results["file"]:
    pb.send_file(fileTitle, results["file"])
  else:
    pb.send_note(title,body)


if __name__ == "__main__":
  try:
    main(sys.argv[1:])
  except KeyboardInterrupt:
    sys.exit()
