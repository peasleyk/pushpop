Overview
=========

A simple script to send a pushbullet message. I wrote it as I wanted to know when some large files were done copying, or when I was writing to a disk. Basically any long process, e.g. 'some long command; pushpop'

How to install
----------

Requires python-magic. Use ```pip install --user python-magic```.

To use, edit pushpop and enter your pushbullet api key in 'KEY = '.
As I use this by adding it to by path, I don't really want a config file for one value and pipfile for one dependency

You can create an account and get a key here - https://www.pushbullet.com/

After adding your key and installing python-magic, make this file executable, ```chmod +x pushpup```, then add it to your path or as an alias.

How to use
-----------

By default you can type 'pushpop' and a message will be sent

```
usage: pushpop.py [-h] [-f FILE] [-t TITLE] [-b BODY]

optional arguments:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  send a file to be uploaded with pushbullet
  -t TITLE, --title TITLE
                        set the push title
  -b BODY, --body BODY  set the push body
```